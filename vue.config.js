module.exports = {
  css: {
    loaderOptions: {
      sass: {
        includePaths: ['~@/scss'],
        data: '@import "@/scss/app.scss";',
      },
    },
  },
};
